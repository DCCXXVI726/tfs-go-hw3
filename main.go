package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"time"
)

type Candle struct {
	max        float64
	min        float64
	openPrice  float64
	closePrice float64
	ts         time.Time
}

type Line struct {
	ticker string
	price  float64
	ts     time.Time
}

func main() {
	const (
		interval1     = 5
		interval2     = 30
		interval3     = 240
		timeOut       = 5
		defaultString = ""
	)

	arrayInterval := []int{interval1, interval2, interval3}
	chanStart := make(chan string)
	chanErrs := make(chan error)
	chansWrite := make([]chan map[string]Candle, 0)

	chansLine := firstStage(chanStart, chanErrs)
	for i, chanLine := range chansLine {
		chansWrite = append(chansWrite, secondStage(chanLine, arrayInterval[i]))
	}

	chanEnd := thirdStage(chansWrite, chanErrs)

	filePtr := flag.String("file", defaultString, "file name")
	flag.Parse()

	if *filePtr != defaultString {
		chanStart <- *filePtr
	} else {
		log.Fatalf("wrong params")
	}
	select {
	case <-chanEnd:
		fmt.Println("everything is fine")
	case <-time.After(timeOut * time.Second):
		log.Fatal("time is out")
	case err := <-chanErrs:
		log.Fatal(err)
	}
}

func thirdStage(chansWrite []chan map[string]Candle, chanErrs chan error) chan interface{} {
	chanEnd := make(chan interface{})

	go func() {
		defer close(chanEnd)

		const (
			interval5m   = 0
			interval30m  = 1
			interval240m = 2
		)

		flag1 := true
		flag2 := true
		flag3 := true

		file1, err := os.Create("candles_5min.csv")
		if err != nil {
			chanErrs <- fmt.Errorf("can't create candles_5min.csv: %s", err)
			return
		}
		defer file1.Close()
		writer1 := csv.NewWriter(file1)

		file2, err := os.Create("candles_30min.csv")
		if err != nil {
			chanErrs <- fmt.Errorf("can't create candles_30min.csv: %s", err)
			return
		}
		defer file2.Close()
		writer2 := csv.NewWriter(file2)

		file3, err := os.Create("candles_240min.csv")
		if err != nil {
			chanErrs <- fmt.Errorf("can't create candles_240min.csv: %s", err)
			return
		}
		defer file3.Close()
		writer3 := csv.NewWriter(file3)

		for flag1 || flag2 || flag3 {
			select {
			case candleMap1 := <-chansWrite[interval5m]:
				flag1 = handleCandle(candleMap1, writer1, chanErrs)
			case candleMap2 := <-chansWrite[interval30m]:
				flag2 = handleCandle(candleMap2, writer2, chanErrs)
			case candleMap3 := <-chansWrite[interval240m]:
				flag3 = handleCandle(candleMap3, writer3, chanErrs)
			}
		}
		chanEnd <- nil
	}()

	return chanEnd
}

func secondStage(chanLine chan Line, delay int) chan map[string]Candle {
	chanWrite := make(chan map[string]Candle)

	go func() {
		defer close(chanWrite)

		const (
			year      = 2020
			month     = time.March
			dayStart  = 14
			hourStart = 7
			min       = 0
			sec       = 0
			nsec      = 0
		)

		now := time.Date(year, month, dayStart, hourStart, min, sec, nsec, time.UTC)
		result := make(map[string]Candle)

		for line := range chanLine {
			if line.ts.Hour() < hourStart {
				continue
			}

			for {
				if diff(now, line.ts) < delay && isSameDay(now, line.ts) {
					updateCandle(line, result, now)

					break
				} else {
					if result != nil {
						chanWrite <- result
					}
					result = make(map[string]Candle)
					if isSameDay(now, line.ts) {
						now = now.Add(time.Minute * time.Duration(delay))
					} else {
						now = setDate(line.ts)
					}
				}
			}
		}

		if result != nil {
			chanWrite <- result
		}
		chanWrite <- nil
	}()

	return chanWrite
}

func firstStage(chanStart chan string, chanErrs chan error) []chan Line {
	chansLine := []chan Line{make(chan Line), make(chan Line), make(chan Line)}

	go func() {
		for _, chanLine := range chansLine {
			defer close(chanLine)
		}

		var (
			line Line
			name string
		)

		name = <-chanStart
		file, err := os.Open(name)

		if err != nil {
			chanErrs <- fmt.Errorf("can't open file %s: %s", name, err)
			return
		}

		defer file.Close()
		reader := csv.NewReader(file)

		for {
			record, err := reader.Read()
			if err == io.EOF {
				break
			}

			if err != nil {
				chanErrs <- fmt.Errorf("can't read file: %s", err)
				return
			}

			line, err = encode(record)
			if err != nil {
				chanErrs <- fmt.Errorf("can't encode: %s", err)
				return
			}

			for _, chanLine := range chansLine {
				chanLine <- line
			}
		}
	}()

	return chansLine
}

func encode(record []string) (Line, error) {
	var (
		line Line
		err  error
	)

	const (
		ticker         = 0
		price          = 1
		ts             = 3
		numberOfParams = 4
	)

	if len(record) != numberOfParams {
		return line, fmt.Errorf("wrong number of params in %s", record)
	}

	line.ticker = record[ticker]

	line.price, err = strconv.ParseFloat(record[price], 64)
	if err != nil {
		return line, fmt.Errorf("can't parse float %s: %s", record[price], err)
	}

	const form = "2006-01-02 15:04:05.000000"

	line.ts, err = time.Parse(form, record[ts])
	if err != nil {
		return line, fmt.Errorf("can't parse time %s: %s", record[ts], err)
	}

	return line, nil
}

func diff(before time.Time, after time.Time) int {
	delay := (after.Hour()-before.Hour())*60 + after.Minute() - before.Minute()
	return (int)(math.Abs((float64)(delay)))
}

func updateCandle(line Line, result map[string]Candle, now time.Time) {
	candle, ok := result[line.ticker]
	if ok {
		if candle.max < line.price {
			candle.max = line.price
		}

		if candle.min > line.price {
			candle.min = line.price
		}

		candle.closePrice = line.price
		result[line.ticker] = candle
	} else {
		result[line.ticker] = Candle{max: line.price, min: line.price, openPrice: line.price, closePrice: line.price, ts: now}
	}
}

func isSameDay(first time.Time, second time.Time) bool {
	if first.Year() == second.Year() && first.Month() == second.Month() && first.Day() == second.Day() {
		return true
	}

	return false
}

func setDate(ts time.Time) time.Time {
	const (
		hour = 7
		min  = 0
		sec  = 0
		nsec = 0
	)

	return time.Date(ts.Year(), ts.Month(), ts.Day(), hour, min, sec, nsec, time.UTC)
}

func parseCandle(writer *csv.Writer, candle Candle, ticker string) error {
	err := writer.Write([]string{ticker, candle.ts.Format(time.RFC3339),
		fmt.Sprint(candle.openPrice), fmt.Sprint(candle.max), fmt.Sprint(candle.min), fmt.Sprint(candle.closePrice)})
	if err != nil {
		return fmt.Errorf("problem with write: %s", err)
	}

	writer.Flush()

	return nil
}

func handleCandle(candleMap map[string]Candle, writer *csv.Writer, chanErrs chan error) bool {
	if candleMap == nil {
		return false
	}

	for ticker, candle := range candleMap {
		err := parseCandle(writer, candle, ticker)
		if err != nil {
			chanErrs <- fmt.Errorf("can't parse candle %s: %s", ticker, err)
			return false
		}
	}

	return true
}
